package uz.pdp.appmongodbspring.collection;

import lombok.Data;

@Data
public class Address {

    private String street;

    private String suite;

    private String city;

    private String zipcode;

}

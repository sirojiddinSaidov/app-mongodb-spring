package uz.pdp.appmongodbspring.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appmongodbspring.collection.User;
import uz.pdp.appmongodbspring.repository.UserRepository;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    @GetMapping
    public HttpEntity<Slice<User>> list(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "name") String sort,
            @RequestParam(defaultValue = "ASC") Sort.Direction sortType,
            @RequestParam(defaultValue = "") String search) {

        Pageable pageable = PageRequest.of(page, size, sortType, sort);

//        Slice<User> userPage = userRepository.findAllByNameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrAddress_CityContainingIgnoreCase(search, search, search, pageable);


        Slice<User> userSlice = userRepository.findAllByNameOrEmailOrCity(search, pageable);
        return ResponseEntity.ok(userSlice);
    }

    @GetMapping("/{id}")
    public HttpEntity<User> one(@PathVariable String id) {
        User user = userRepository.findById(id).orElseThrow(RuntimeException::new);
        return ResponseEntity.ok(user);
    }

    @PostMapping
    public HttpEntity<User> add(@RequestBody User user) {
        if (userRepository.existsByEmail(user.getEmail()))
            throw new RuntimeException("OKa bu email bor-u");

        userRepository.save(user);
        return ResponseEntity.ok(user);
    }


    @PutMapping("/{id}")
    public HttpEntity<User> edit(@PathVariable String id,
                                 @RequestBody User updatingUser) {

        if (userRepository.existsByObjIdNotAndEmail(id, updatingUser.getEmail()))
            throw new RuntimeException("Oka yana o'sha email bor-u");

        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("Oka bunday user yo'qku"));

        user.setName(updatingUser.getName());
        user.setEmail(updatingUser.getEmail());
        user.setUsername(updatingUser.getUsername());

        userRepository.save(user);
        return ResponseEntity.ok(user);
    }


    @DeleteMapping("/{id}")
    public HttpEntity<Boolean> delete(@PathVariable String id) {
        userRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
